package com.example.on_fly_movie.utils

enum class MovieType(var type: String) {
    UP_COMING("upcoming"),
    POPULAR("popular"),
    TOP_RATED("top_rated"),
    NOW_PLAYING("now_playing"),
    LATEST("latest");

    override fun toString(): String {
        return type // working!
    }
}


