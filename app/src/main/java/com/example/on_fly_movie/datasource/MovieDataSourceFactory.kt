package com.example.on_fly_movie.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.example.on_fly_movie.models.BaseMovieModel
import com.example.on_fly_movie.repository.BaseRepository
import kotlinx.coroutines.CoroutineScope

class MovieDataSourceFactory(private val repository: BaseRepository,private val scope: CoroutineScope, private val movieType: String?): DataSource.Factory<Int, BaseMovieModel>() {

    private val source = MutableLiveData<MovieDataSource>()
    var movieTypes = movieType
    override fun create(): DataSource<Int, BaseMovieModel> {

        val source = MovieDataSource(scope,repository,1, null,movieTypes)
        this.source.postValue(source)
        return source

    }

}

class SearchDataSourceFactory(private val repository: BaseRepository,private val scope: CoroutineScope, private val qwery: String?): DataSource.Factory<Int, BaseMovieModel>(){
    private val source = MutableLiveData<SearchDataSource>()
    var queryData  = qwery
    override fun create(): DataSource<Int, BaseMovieModel> {

        val source = SearchDataSource(scope,repository,1, null, queryData)
        this.source.postValue(source)
        return source

    }

    fun clearSource(){
        val source = SearchDataSource(scope,repository,null, null, "")
        //this.source.postValue(source)

         source.invalidate()
    }
}