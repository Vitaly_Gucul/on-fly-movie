package com.example.on_fly_movie.models

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
open class BaseMovieModel(
//    val id: Int?,
//    val poster_path: String?,
//    val backdrop_path: String?,
//    val original_title: String?,
//    val title: String?
) : Parcelable {
    val id: Int? = null
    val poster_path: String? = null
    val backdrop_path: String? = null
    val original_title: String? = null
    val title: String? = null
}

data class BaseMovieModelResponse(
    val results: List<BaseMovieModel>
)

@Parcelize
class DetailsMovieModel : BaseMovieModel(), Parcelable {
    val overview: String? = null
    val release_date: String? = null
    val runtime: Int? = null
    val status: String? = null
    val tagline: String? = null
    val video: Boolean? = null
    val vote_average: Double? = null
    val vote_count: Int? = null
}

@JsonClass(generateAdapter = true)
data class ImageDataResponse(
    @Json(name = "backdrops")
    val backdrops: List<Backdrop>,
    @Json(name = "id")
    val id: Int?,
    @Json(name = "posters")
    val posters: List<Poster?>?
) {
    @JsonClass(generateAdapter = true)
    data class Poster(
        @Json(name = "aspect_ratio")
        val aspectRatio: Double?,
        @Json(name = "file_path")
        val filePath: String?,
        @Json(name = "height")
        val height: Int?,
        @Json(name = "iso_639_1")
        val iso6391: String?,
        @Json(name = "vote_average")
        val voteAverage: Double?,
        @Json(name = "vote_count")
        val voteCount: Int?,
        @Json(name = "width")
        val width: Int?
    )

}

@JsonClass(generateAdapter = true)
data class Backdrop(
    @Json(name = "aspect_ratio")
    val aspectRatio: Double?,
    @Json(name = "file_path")
    val filePath: String?,
    @Json(name = "height")
    val height: Int?,
    @Json(name = "iso_639_1")
    val iso6391: String?,
    @Json(name = "vote_average")
    val voteAverage: Double?,
    @Json(name = "vote_count")
    val voteCount: Int?,
    @Json(name = "width")
    val width: Int?
)
data class PosterResponse(val list: List<ImageDataResponse.Poster>)