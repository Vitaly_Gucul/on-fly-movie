package com.example.on_fly_movie.repository

import com.example.on_fly_movie.api.IMovieDbApi
import com.example.on_fly_movie.models.*


class HomeRepo  (private  val api: IMovieDbApi) : BaseRepository() {

    suspend fun getTrendingMovie() : MutableList<BaseMovieModel>? {
        val trendingMovie = safeApiCall(
            call = { this.api.getTrendingMovieAsync("movie", "week").await() },
            errorMessage = "Error Fetching trending Movies"
        )

        return trendingMovie?.results?.toMutableList()
    }

    suspend fun getUpComingMovie() : MutableList<BaseMovieModel>? {
        val upComingMovie = safeApiCall(
            call = { this.api.getUpcomingMoviesAsync(1, 1, "ru-RU").await() },
            errorMessage = "Error Fetching UpComing Movies"
        )

        return upComingMovie?.results?.toMutableList()
    }

    suspend fun getPopularMovies() : MutableList<BaseMovieModel>?{
        val popularMovie = safeApiCall(
            call = {this.api.getPopularMovieAsync(1,1,"ru-RU").await()},
            errorMessage = "Error Fetching Popular Movies"
        )
        return popularMovie?.results?.toMutableList()

    }

    suspend fun nowPlayingMovies() : MutableList<BaseMovieModel>?{
        val nowPlaying = safeApiCall(
            call = {this.api.getNowPlayingMoviesAsync(1,1,"ru-RU").await()},
            errorMessage = "Error Fetching Now Playing Movies"
        )
        return nowPlaying?.results?.toMutableList()
    }

    suspend fun getMovieById(movieId: Int?): DetailsMovieModel? {
        val movie = safeApiCall(
            call = {this.api.getMovieByIdAsync(movieId,"ru-RU").await()},
            errorMessage = "Error Fetching Movie by ID"
        )
        return movie
    }

    suspend fun getDetailsMovieImages(movieId: Int?) : ImageDataResponse?{
        val images = safeApiCall(
            call = {this.api.getDetailsMovieImagesAsync(movieId).await()},
            errorMessage = "Error Fetching Details Movie images"
        )
        return images
    }

    suspend fun searchMovieRepo(query: String) :  MutableList<BaseMovieModel>?{
        val dataSearch = safeApiCall(
            call = {this.api.searchMoviesAsync(query,1,1,"ru-RU").await()},
            errorMessage = "Error Fetching Search Movies"
        )
        return dataSearch?.results?.toMutableList()
    }

    suspend fun getMovieCredits(movieId: Int?) : MutableList<CastList>{
        val credits = safeApiCall(
            call = {this.api.getMovieCreditsAsync(movieId, "ru-RU").await()},
            errorMessage = "Error Fetching credits in details movies"
        )
        return credits?.cast!!
    }
}