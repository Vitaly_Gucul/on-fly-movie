package com.example.on_fly_movie.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.os.bundleOf
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.on_fly_movie.BuildConfig
import com.example.on_fly_movie.R
import com.example.on_fly_movie.handlers.GladeHelper
import com.example.on_fly_movie.models.BaseMovieModel
import kotlinx.android.synthetic.main.fragment_home.*

class MoviesAdapter :  PagedListAdapter<BaseMovieModel, MoviesAdapter.MovieViewHolder>(DIFF_CALLBACK) {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MoviesAdapter.MovieViewHolder {
        return MovieViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.movies_list_recycler,
                parent,
                false
            )
        )
    }


    override fun onBindViewHolder(holder: MoviesAdapter.MovieViewHolder, position: Int) {
        val movie: BaseMovieModel? = getItem(position)
        holder.movies_cards!!.animation =
            AnimationUtils.loadAnimation(holder.itemView.context, R.anim.recycle_anim)
        holder.bind(movie)

    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<BaseMovieModel>() {
            override fun areItemsTheSame(oldItem: BaseMovieModel, newItem: BaseMovieModel): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: BaseMovieModel,
                newItem: BaseMovieModel
            ): Boolean {
                return oldItem.equals(newItem)
            }
        }
    }

    inner class MovieViewHolder(itemView: View) :  RecyclerView.ViewHolder(itemView) {
        private val glideHelper = GladeHelper()
        private val urlToImage = BuildConfig.URL_IMAGE_W500

        private var upComingPoster: ImageView? = null
        private var titleMovie: TextView? = null
        var movies_cards: CardView? = null
        //private var r_ritle: TextView? = itemView.findViewById(R.id.r_ritle)

        init {
            upComingPoster = itemView.findViewById(R.id.movie_poster)
            titleMovie = itemView.findViewById(R.id.title_movie)
            movies_cards = itemView.findViewById(R.id.movies_cards)

        }


        fun bind(data: BaseMovieModel?) {

            val movieId: Int? = data?.id
            val bundle = Bundle()
            bundle.putInt("movieId", movieId!!)

            glideHelper.loadMovieImage(itemView, urlToImage + data.backdrop_path, upComingPoster)
            titleMovie?.text = data.title

            itemView.setOnClickListener{
                println("movie ID: ${data.id}")

                it.findNavController().navigate(
                    R.id.action_nav_moviesFragment_to_detailsMovieFragment,
                    bundle
                )

            }

        }

    }





}