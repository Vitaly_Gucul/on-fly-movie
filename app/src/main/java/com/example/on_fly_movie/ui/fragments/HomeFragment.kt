package com.example.on_fly_movie.ui.fragments

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ScrollView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation.findNavController
import androidx.viewpager.widget.ViewPager
import com.example.on_fly_movie.BuildConfig
import com.example.on_fly_movie.R
import com.example.on_fly_movie.adapters.HomeViewPagerAdapter
import com.example.on_fly_movie.handlers.GladeHelper
import com.example.on_fly_movie.utils.DepthTransformation
import com.example.on_fly_movie.utils.MovieType
import com.example.on_fly_movie.utils.autoCleared
import com.example.on_fly_movie.viewmodels.HomeViewModel
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.*


class HomeFragment : Fragment() {

    //private val viewModel: HomeViewModel by viewModels()
    private val viewModel: HomeViewModel by lazy {
        val activity = requireNotNull(this.activity) {
            "You can only access the viewModel after onActivityCreated()"
        }
        ViewModelProviders.of(this, HomeViewModel.Factory(activity.application))
            .get(HomeViewModel::class.java)
    }
    var scrollView by autoCleared<ScrollView>()
    //val viewModel by viewModels { SavedStateVMFactory(this) }
    private val urlToImage = BuildConfig.URL_IMAGE_W500

    private  lateinit var viewPager: ViewPager
    private  lateinit var tabLayout: TabLayout
    private  lateinit var popularCardItem: CardView


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment


        return inflater.inflate(R.layout.fragment_home, container, false)
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val isNetworkOnline = isNetworkAvailable(requireContext())

        if(!isNetworkOnline){
            viewModel.cancelAllRequests()
            val text = "No internet connection!"
            val duration = Toast.LENGTH_SHORT

            val toast = Toast.makeText(context, text, duration)
            toast.show()

        }


        val glideHelper = GladeHelper()

        scrollView = view.findViewById(R.id.mScrollView)
        viewPager = view.findViewById(R.id.trending_view_pager)
        tabLayout = view.findViewById(R.id.tab_layout)



        popularCardItem = view.findViewById(R.id.card_view_up_coming)


        val randomNumber: Int = Random().nextInt(20)




        // Adapter
        viewModel.trendingMovieImageLiveData.observe(viewLifecycleOwner, Observer {
            val adapter = HomeViewPagerAdapter(context, it)

            this.viewPager.adapter = adapter
            this.tabLayout.setupWithViewPager(viewPager)

            val depthTransformation = DepthTransformation()
            this.viewPager.setPageTransformer(true, depthTransformation);
            this.viewPager.clipToPadding = false;


        })



        viewModel.upComingMovieImageLiveData.observe(viewLifecycleOwner, Observer {

            glideHelper.loadImage(
                view = view,
                urlToImage = urlToImage,
                pathToImage = it[0].backdrop_path.toString(),
                intoView = menu_up_coming
            )

        })

        //TopRated
        viewModel.populardLiveData.observe(viewLifecycleOwner, Observer {

            glideHelper.loadImage(
                view = view,
                urlToImage = urlToImage,
                pathToImage = it[randomNumber].backdrop_path.toString(),
                intoView = menu_up_popular
            )

            glideHelper.loadImage(
                view = view,
                urlToImage = urlToImage,
                pathToImage = it[2].backdrop_path.toString(),
                intoView = menu_top_rated
            )
        })

        viewModel.nowPlayingMoviesLiveData.observe(viewLifecycleOwner, Observer {
            glideHelper.loadImage(
                view = view,
                urlToImage = urlToImage,
                pathToImage = it[3].backdrop_path.toString(),
                intoView = menu_now_playing
            )
        })
        val clickListener = View.OnClickListener { view ->

            when (view.id) {
                R.id.menu_up_coming -> findNavController(view).navigate(
                    R.id.action_homeFragment_to_MoviesFragment,
                    bundleOf("movieType" to MovieType.UP_COMING)
                )
                R.id.menu_up_popular -> findNavController(view).navigate(
                    R.id.action_homeFragment_to_MoviesFragment,
                    bundleOf("movieType" to MovieType.POPULAR)
                )
                R.id.menu_top_rated -> findNavController(view).navigate(
                    R.id.action_homeFragment_to_MoviesFragment,
                    bundleOf("movieType" to MovieType.TOP_RATED)
                )
                R.id.menu_now_playing -> findNavController(view).navigate(
                    R.id.action_homeFragment_to_MoviesFragment,
                    bundleOf("movieType" to MovieType.NOW_PLAYING)
                )
            }
        }

        menu_up_coming.setOnClickListener(clickListener)
        menu_up_popular.setOnClickListener(clickListener)
        menu_top_rated.setOnClickListener(clickListener)
        menu_now_playing.setOnClickListener(clickListener)


    }


    fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        // For 29 api or above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork) ?: return false
            return when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ->    true
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) ->   true
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ->   true
                else ->     false
            }
        }
        // For below 29 api
        else {
            if (connectivityManager.activeNetworkInfo != null && connectivityManager.activeNetworkInfo!!.isConnectedOrConnecting) {
                return true
            }
        }
        return false
    }

    override fun onStart() {
        super.onStart()
        viewModel.fetchTrendingMovieImage()
        viewModel.fetchUpComingMovieImage()
        viewModel.fetchPopularMovieImage()
        viewModel.fetchNowPlayingMovies()
    }

    override fun onPause() {
        super.onPause()
        viewModel.cancelAllRequests()
    }

    override fun onResume() {
        super.onResume()


    }
    override fun onDestroyView() {
        super.onDestroyView()
        //recyclerView.removeView()
        //viewModelStore.clear()
        mScrollView.removeAllViews()

    }
}