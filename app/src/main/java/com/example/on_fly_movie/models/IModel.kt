package com.example.on_fly_movie.models

import androidx.lifecycle.LiveData
import com.example.on_fly_movie.viewstates.IIntent
import com.example.on_fly_movie.viewstates.IState
import kotlinx.coroutines.channels.Channel


interface IModel<S: IState, I: IIntent> {
    val intents: Channel<I>
    val state: LiveData<S>
}