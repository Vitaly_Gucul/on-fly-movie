package com.example.on_fly_movie.ui.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.on_fly_movie.R
import com.example.on_fly_movie.adapters.SearchAdapter
import com.example.on_fly_movie.utils.OnFocusLostListener
import com.example.on_fly_movie.viewmodels.SearchViewModel
import kotlinx.android.synthetic.main.fragment_search.*

class SearchFragment : BaseFragment() {

    private lateinit var viewModel: SearchViewModel

    lateinit var recyclerView: RecyclerView
    lateinit var adapter: SearchAdapter


    override fun provideYourFragmentView(
        inflater: LayoutInflater?,
        parent: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view: View = inflater!!.inflate(R.layout.fragment_search, parent, false)

        viewModel = ViewModelProvider(this).get(SearchViewModel::class.java)
        recyclerView = view.findViewById(R.id.rv_search_recycler)

        initRecycle()
        viewModel.searchBuilder.observe(viewLifecycleOwner, Observer {

            adapter.submitList(it)

        })

        val actionBar = (activity as AppCompatActivity).supportActionBar
        actionBar?.title = ""

        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }


    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        recyclerView.setOnTouchListener(object : View.OnTouchListener{
            @SuppressLint("ClickableViewAccessibility")
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)

                return  false
            }

        })

        search_input.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

                adapter.submitList(null)

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                adapter.submitList(null)


            }

            override fun afterTextChanged(s: Editable?) {

                viewModel.fetchSearchMovies(s.toString())

                // viewModel.searchDataSource.queryData = s.toString()
                //viewModel.fetchSearchMovies(s.toString())
//                viewModel.searchDataSource.queryData = s.toString()
//                adapter.submitList(null)



            }

        })


    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //viewModel.searchDataSource.queryData = "harry "


    }

    private fun initRecycle() {
        adapter = SearchAdapter()
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter
    }
}