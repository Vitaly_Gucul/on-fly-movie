package com.example.on_fly_movie

import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.ActivityNavigator
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var navController: NavController
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var listener: NavController.OnDestinationChangedListener


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navController = Navigation.findNavController(this, R.id.nav_host)
        // drawerLayout = findViewById(R.id.drawer_layout)
        bttm_nav.setupWithNavController(navController)

        setSupportActionBar(myToolbar)
        appBarConfiguration = AppBarConfiguration(navController.graph)


        setupActionBarWithNavController(navController, appBarConfiguration)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)


        setupActionBarWithNavController(navController, appBarConfiguration)



        /**
         * change color of ActionBar
         */
        listener =
            NavController.OnDestinationChangedListener { controller, destination, arguments ->

                //supportActionBar?.title = "QQQ"
//                supportActionBar?.title = when (destination.id) {
//                    R.id.homeFragment -> "My title"
//                    R.id.nav_moviesFragment -> "Movies"
//                    else -> "Default title"
//                }

                if (destination.id == R.id.nav_moviesFragment) {
                    supportActionBar?.setBackgroundDrawable(ColorDrawable( ContextCompat.getColor(this, R.color.transparent)))
                    supportActionBar?.show()

                }
                if(destination.id == R.id.searchFragment){
                    supportActionBar?.hide()

                }
                if(destination.id == R.id.detailsMovieFragment) {
                    supportActionBar?.show()
                }

                if(destination.id == R.id.settingsFragment){
                    supportActionBar?.hide()
                }
                if (destination.id == R.id.homeFragment) {
                    //supportActionBar?.setBackgroundDrawable(ColorDrawable(getColor(R.color.main_color_grey)))

                    supportActionBar?.hide()

                } else if (destination.id == R.id.testFragment) {
                    //supportActionBar?.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(this, R.color.teal_700)))
                    supportActionBar?.hide()

                }

            }

    }

    override fun onResume() {
        super.onResume()
        navController.addOnDestinationChangedListener(listener)
    }

    override fun onPause() {
        super.onPause()
        navController.removeOnDestinationChangedListener(listener)

    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host)
        return navController.navigateUp(appBarConfiguration) ||
                return super.onSupportNavigateUp()
    }

    override fun finish() {
        super.finish()
        ActivityNavigator.applyPopAnimationsToPendingTransition(this)
    }

    private fun enableFullScreenMode() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}