package com.example.on_fly_movie.viewstates

import com.example.on_fly_movie.models.DetailsMovieModel


data class DetailMovieState(
    val success: DetailsMovieModel? = null,
    val isLoading: Boolean = false,
    val errorMessage: String? = null

): IState

data class ImagesDetailsMovieState(
    val  success: List<DetailsMovieModel>? = emptyList(),
    val isLoading: Boolean = false,
    val errorMessage: String? = null
) : IState