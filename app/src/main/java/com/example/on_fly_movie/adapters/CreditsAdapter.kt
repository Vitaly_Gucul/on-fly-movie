package com.example.on_fly_movie.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.on_fly_movie.BuildConfig
import com.example.on_fly_movie.R
import com.example.on_fly_movie.models.CastList

class CreditsAdapter(private val context: Context?, private val credits: MutableList<Map.Entry<String?, List<CastList>>>) :
    RecyclerView.Adapter<CreditsAdapter.CreditsViewHolder>() {


    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = credits.size


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): CreditsViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.credits_movie_items, viewGroup, false)

        return CreditsViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: CreditsViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.textView.text = credits[position].key

        for (i in credits[position].value){
            Glide.with(context!!)
                .load(BuildConfig.URL_IMAGE_W500 + i.profile_path)
                .into(viewHolder.actorPoster)

        }


    }


    class CreditsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView: TextView = view.findViewById(R.id.actor_original_name)
        val actorPoster: ImageView = view.findViewById(R.id.actor_poster)


    }
}