package com.example.on_fly_movie.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.example.on_fly_movie.BuildConfig
import com.example.on_fly_movie.R
import com.example.on_fly_movie.models.BaseMovieModel

/**
 * Adapter for carousel
 */
class HomeViewPagerAdapter(private val context: Context?, private val model: List<BaseMovieModel>) : PagerAdapter() {
    private val urlToImage = BuildConfig.URL_IMAGE_W500

    override fun getCount(): Int {
       return model.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
     return  view == `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
      // val imageView = ImageView(context)

        val layoutInflater = LayoutInflater.from(context)
        val view: View = layoutInflater.inflate(R.layout.home_trending_list, container, false)
        val image = view.findViewById<ImageView>(R.id.menu_trending)
        val title = view.findViewById<TextView>(R.id.trending_title)

        title.text = model[position].title

        Glide.with(context!!)
            .load(urlToImage + model[position].backdrop_path)
            .into(image)

        container.addView(view, 0)

        return  view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {

        container.removeView(`object` as View)
    }
}