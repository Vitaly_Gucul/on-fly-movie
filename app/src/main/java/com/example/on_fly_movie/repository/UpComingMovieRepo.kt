package com.example.on_fly_movie.repository

import com.example.on_fly_movie.api.IMovieDbApi
import com.example.on_fly_movie.models.BaseMovieModel

class UpComingMovieRepo (private  val api: IMovieDbApi) : BaseRepository() {


    suspend fun getMoviesPagedList(movieType: String?) : MutableList<BaseMovieModel>? {
        println("movieType-> $movieType")
        val movies= safeApiCall(
            call = { this.api.getPagedMoviesAsync(movieType,1, 1,"ru-Ru").await() },
            errorMessage = "Error Fetching from Repository MoviesPagedList"
        )

        return movies?.results?.toMutableList()
    }

}