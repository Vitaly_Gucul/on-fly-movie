package com.example.on_fly_movie.repository

import android.util.Log
import com.example.on_fly_movie.handlers.ResultHandler
import retrofit2.Response
import java.io.IOException

open class BaseRepository {
    suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>, errorMessage: String): T? {

        val result : ResultHandler<T> = safeApiResult(call,errorMessage)
        var data : T? = null

        when(result) {

            is ResultHandler.Success ->
                data = result.data

            is ResultHandler.Error -> {
                Log.d("BaseRepository", "$errorMessage & Exception - ${result.exception}")
            }
        }


        return data

    }

    private suspend fun <T: Any> safeApiResult(call: suspend ()-> Response<T>, errorMessage: String) : ResultHandler<T> {
        val response = call.invoke()
        if(response.isSuccessful) return ResultHandler.Success(response.body()!!)

        return ResultHandler.Error(IOException("Error Occurred during getting safe Api result, Custom ERROR - $errorMessage"))
    }
}