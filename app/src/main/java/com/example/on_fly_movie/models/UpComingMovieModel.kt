package com.example.on_fly_movie.models

class UpComingMovieModel (
    val id: Int?,
    val poster_path: String?,
    val original_title: String?,
    val title: String?
)

data class UpComingResponse(
    val results: List<UpComingMovieModel>
)