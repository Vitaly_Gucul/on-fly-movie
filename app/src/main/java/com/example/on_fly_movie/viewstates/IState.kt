package com.example.on_fly_movie.viewstates

import android.view.View

interface IState {}

interface IIntent {}

interface IView<S: IState, T: View> {
    fun render(state: S, view: T)
}