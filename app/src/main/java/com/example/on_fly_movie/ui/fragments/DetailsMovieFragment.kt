package com.example.on_fly_movie.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.example.on_fly_movie.BuildConfig
import com.example.on_fly_movie.R
import com.example.on_fly_movie.adapters.CreditsMovieAdapter
import com.example.on_fly_movie.adapters.DetailsMovieImageAdapter
import com.example.on_fly_movie.handlers.GladeHelper
import com.example.on_fly_movie.models.CastList
import com.example.on_fly_movie.models.RecyclerItem
import com.example.on_fly_movie.utils.HeaderItemDecoration
import com.example.on_fly_movie.viewmodels.DetailsMovieViewModel
import com.example.on_fly_movie.viewstates.DetailMovieState
import com.example.on_fly_movie.viewstates.DetailsMovieIntent
import com.example.on_fly_movie.viewstates.IView
import kotlinx.android.synthetic.main.details_movie_fragment.*
import kotlinx.coroutines.launch
import kotlin.math.abs


class DetailsMovieFragment : Fragment(), IView<DetailMovieState, View> {

    private lateinit var fab_open: Animation
    private lateinit var fab_close: Animation
    private lateinit var fab_clock: Animation
    private lateinit var fab_anticlock: Animation

    private lateinit var viewPager: ViewPager

    //private lateinit var creditsAdapter: CreditsAdapter
    private lateinit var creditsAdapter: CreditsMovieAdapter
    private lateinit var creditsRecyclerView: RecyclerView


    var isOpen = false

    //private lateinit var viewModel: DetailsMovieViewModel
    private val viewModel: DetailsMovieViewModel by lazy {
        val activity = requireNotNull(this.activity) {
            "You can only access the viewModel after onActivityCreated()"

        }
        ViewModelProviders.of(
            this,
            DetailsMovieViewModel.Factory(activity.application, this.movieId)
        )
            .get(DetailsMovieViewModel::class.java)
    }

    private var movieId: Int? = null
    private val urlToImage = BuildConfig.URL_IMAGE_W500


    override fun onAttach(context: Context) {
        super.onAttach(context)

        movieId = arguments?.getInt("movieId")
        println("details movie $movieId")


        // Fetching data when the application launched
        lifecycleScope.launch {
            viewModel.intents.send(DetailsMovieIntent.FetchDetailsMovie)
            viewModel.intents.send(DetailsMovieIntent.FetchImagesGallery)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.details_movie_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        creditsRecyclerView = credits_recyclerView
        fab_close = AnimationUtils.loadAnimation(context, R.anim.fab_close_animation)
        fab_open = AnimationUtils.loadAnimation(context, R.anim.fab_open_animation)
        fab_clock = AnimationUtils.loadAnimation(context, R.anim.fab_rotate_clock)
        fab_anticlock = AnimationUtils.loadAnimation(context, R.anim.fab_rotate_anti_clock)

        viewPager = view.findViewById(R.id.details_view_pager)





        fab_main.setOnClickListener {
            if (isOpen) {

                fab1.startAnimation(fab_close);
                fab2.startAnimation(fab_close);
                fab_main.startAnimation(fab_anticlock);
                fab2.isClickable = false
                fab1.isClickable = false
                isOpen = false


            } else {
                fab2.startAnimation(fab_open);
                fab1.startAnimation(fab_open);
                fab_main.startAnimation(fab_clock);
                fab1.isClickable = true
                fab2.isClickable = true
                isOpen = true;
            }
        }
        // Observing the state
        viewModel.state.observe(viewLifecycleOwner, Observer {
            render(it, view)
        })


        initCredits(movieId)
        initImagesGallery(movieId)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //shimmerFrameLayout_details.stopShimmerAnimation()
    }

    override fun render(state: DetailMovieState, view: View) {
        val glideHelper = GladeHelper()
        val actionBar = (activity as AppCompatActivity).supportActionBar
        actionBar?.title = ""


        with(state) {

            when (isLoading) {
                true -> shimmerFrameLayout_details.startShimmerAnimation()
                false -> shimmerFrameLayout_details.stopShimmerAnimation()
            }


            val poster =
                if (success?.poster_path == null) success?.backdrop_path.toString() else success.poster_path.toString()

            glideHelper.loadMovieImage(view, urlToImage + poster, detail_poster_movie)

            detail_movie_overview.text = success?.overview
            details_movie_title.text = success?.title
            details_status.text = success?.status
            details_original_title.text = success?.original_title
            details_release_date.text = success?.release_date

            if (errorMessage != null) {
                Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun initImagesGallery(movieId: Int?) {
        viewModel.fetchImagesGallery(movieId)

        viewModel.imagesLiveData.observe(viewLifecycleOwner, Observer {

            val list = it.backdrops
            val adapter = DetailsMovieImageAdapter(context, list)
            viewPager.adapter = adapter


            val scaleFactor = 0.85f // default 0.85f
            viewPager.pageMargin = -220
            viewPager.offscreenPageLimit = 2
            viewPager.setPageTransformer(
                false
            ) { page, position ->
                page.scaleY = (0.95f - abs(position) * (0.95f - scaleFactor)) // default 1f
                page.scaleX = scaleFactor + abs(position) * (1 - scaleFactor)
            }
        })
    }

    private fun initCredits(movieId: Int?) {
        viewModel.fetchCredits(movieId)
        viewModel.tempLiveCreditsData.observe(viewLifecycleOwner, Observer {

            val data: HashMap<String?, MutableList<CastList>>


            creditsRecyclerView.setHasFixedSize(false)
            //creditsAdapter = CreditsAdapter(context, it)
            creditsAdapter = CreditsMovieAdapter(context, it)
            // creditsRecyclerView.layoutManager = LinearLayoutManager(context)
            creditsRecyclerView.adapter = creditsAdapter

            creditsRecyclerView.addItemDecoration(HeaderItemDecoration(creditsRecyclerView, true) { itemPosition ->

                creditsAdapter.getSectionPosition(itemPosition)
                if (itemPosition == creditsAdapter.getSectionPosition(itemPosition) && itemPosition < creditsAdapter.itemCount) {
                    true

                } else false
            })
            val layoutManager = GridLayoutManager(context, 2, RecyclerView.HORIZONTAL, false)
            layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return when (position) {
                        0 ->  2
                        else -> 1
                    }
                }
            }

            creditsRecyclerView.layoutManager = layoutManager


        })
    }


}