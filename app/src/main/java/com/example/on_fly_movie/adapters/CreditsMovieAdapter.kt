package com.example.on_fly_movie.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.on_fly_movie.BuildConfig
import com.example.on_fly_movie.R
import com.example.on_fly_movie.models.RecyclerItem

class CreditsMovieAdapter(private val context: Context?, list: List<RecyclerItem>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var VIEW_TYPE_SECTION = 1
    private var VIEW_TYPE_ITEM = 2

    var data = list

    override fun getItemViewType(position: Int): Int {
        if (data[position] is RecyclerItem.Section) {
            getSectionPosition(position)
            return VIEW_TYPE_SECTION
        } else {
            return VIEW_TYPE_ITEM
        }

    }
    override fun getItemCount(): Int {
        return data.size
    }

fun getSectionPosition(position: Int) : Int{
    if(data[position] is RecyclerItem.Section){
        return position
    }else{
        return  0
    }

}
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_SECTION) {
            return SectionViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.credits_movie_items,
                    parent,
                    false
                )
            )
        }
        return ContentViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.section_credits, parent, false)
        )
    }


    internal inner class SectionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textView: TextView = itemView.findViewById(R.id.actor_original_name)
        fun bind(item: RecyclerItem.Section) {
            textView.text = item.category
        }
    }

    internal inner class ContentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imagePoster: ImageView = itemView.findViewById(R.id.actor_poster)
        val actor_real_name: TextView = itemView.findViewById(R.id.actor_real_name)


        fun bind(item: RecyclerItem.Title) {
            actor_real_name.text = item.actor_name
            Glide.with(context!!)
                .load(BuildConfig.URL_IMAGE_W500 + item.imageProphile)
                .into(imagePoster)


        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = data[position]
        if (holder is SectionViewHolder && item is RecyclerItem.Section) {

            holder.bind(item)
        }
        if (holder is ContentViewHolder && item is RecyclerItem.Title) {
            holder.bind(item)
        }
    }
}