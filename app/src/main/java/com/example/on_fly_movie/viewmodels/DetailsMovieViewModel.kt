package com.example.on_fly_movie.viewmodels

import android.app.Application
import androidx.lifecycle.*
import com.example.on_fly_movie.factory.ApiFactory
import com.example.on_fly_movie.models.*
import com.example.on_fly_movie.repository.HomeRepo
import com.example.on_fly_movie.viewstates.DetailMovieState
import com.example.on_fly_movie.viewstates.DetailsMovieIntent
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.consumeAsFlow
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

class DetailsMovieViewModel(application: Application, movieId: Int?) : BaseViewModel(application),
    IModel<DetailMovieState, DetailsMovieIntent> {


    override val intents: Channel<DetailsMovieIntent> = Channel(Channel.UNLIMITED)
    private val _state = MutableLiveData<DetailMovieState>().apply {
        value = DetailMovieState()
    }

    private val parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Default

    private val scope = CoroutineScope(coroutineContext)
    private var repository: HomeRepo = HomeRepo(ApiFactory.tmdbApi)

    override val state: LiveData<DetailMovieState>
        get() = _state

    val movieLiveData = MutableLiveData<DetailsMovieModel>()
    val imagesLiveData = MutableLiveData<ImageDataResponse>()
    val creditsLiveData = MutableLiveData<List<RecyclerItem>>()
    //val creditsLiveData = MutableLiveData<CastList>()
    val tempLiveCreditsData= MutableLiveData<List<RecyclerItem>>()

    private val movieId: Int? = movieId

    init {
        handlerIntent()
    }

    //@OptIn(InternalCoroutinesApi::class)
    private fun handlerIntent() {
        scope.launch {
            intents.consumeAsFlow().collect { userIntent ->
                when (userIntent) {
                    DetailsMovieIntent.RefreshMovie -> fetchMovie()
                    DetailsMovieIntent.FetchDetailsMovie -> fetchMovie()
                }
            }
        }
    }

    fun fetchCredits(movieId: Int?) {
        scope.launch {
            val credits = repository.getMovieCredits(movieId)
//            val mData1 : MutableList<Map.Entry<String?, List<CastList>>> = mutableListOf()
//            credits.groupBy { it.known_for_department }.map {
//
//                mData1.add(it)
//
//
//            }
            val group = credits.groupBy { it.known_for_department }.flatMap {(category, foods) ->

                listOf<RecyclerItem>(RecyclerItem.Section(category)) + foods.map { RecyclerItem.Title(it.profile_path, it.name) }
            }


            tempLiveCreditsData.postValue(group)

        }
    }

    fun fetchImagesGallery(movieId: Int?) {
        scope.launch {
            val images = repository.getDetailsMovieImages(movieId)
            imagesLiveData.postValue(images)
        }
    }

    private fun fetchMovie() {
        scope.launch {
            try {
                updateState { it.copy(isLoading = true) }
                updateState {
                    it.copy(
                        isLoading = false,
                        success = repository.getMovieById(movieId)
                    )
                }
            } catch (e: Exception) {
                updateState { it.copy(isLoading = false, errorMessage = e.message) }
            }
        }

    }

    fun fetchGetMovie(movieId: Int?) {
        scope.launch {
            val movie = repository.getMovieById(movieId)
            movieLiveData.postValue(movie)
        }
    }



    private suspend fun updateState(handler: suspend (intent: DetailMovieState) -> DetailMovieState) {
        _state.postValue(handler(state.value!!))
    }

    /**
     * Factory for constructing DetailsMovieViewModel with parameter
     */
    class Factory(val app: Application, val movieId: Int?) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(DetailsMovieViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return DetailsMovieViewModel(app, movieId) as T
            }
            throw IllegalArgumentException("Unable to construct viewmodel")
        }
    }
}