package com.example.on_fly_movie

class Config {

    var tmdbApiKey = BuildConfig.TMDB_API_KEY
    var URL_IMAGE_W500 = "https://image.tmdb.org/t/p/w500"
    var URL_IMAGE_W400 = "https://image.tmdb.org/t/p/w400"
    var URL_IMAGE_W300 = "https://image.tmdb.org/t/p/w300"
    var URL_IMAGE_W200 = "https://image.tmdb.org/t/p/w200"
}