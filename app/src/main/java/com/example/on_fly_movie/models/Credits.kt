package com.example.on_fly_movie.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Credits(
    @Json(name = "id")
    val id: Int? = null,

    @Json(name = "cast")
    val cast: MutableList<CastList>?
)

@JsonClass(generateAdapter = true)
data class CastList(


    @Json(name = "id")
    val id: Int? = null,

    @Json(name = "known_for_department")
    var known_for_department: String? = null,

    @Json(name = "name")
    val name: String? = null,

    @Json(name = "original_name")
    val original_name: String? = null,

    @Json(name = "profile_path")
    val profile_path: String? = null,

    @Json(name = "character")
    val character: String? = null,

    @Json(name = "credit_id")
    val credit_id: String? = null

)

sealed class RecyclerItem {
    data class Title(val imageProphile: String?, val actor_name: String?): RecyclerItem()
    data class Section(val category: String?): RecyclerItem()
}



