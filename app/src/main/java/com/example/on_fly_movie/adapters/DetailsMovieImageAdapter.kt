package com.example.on_fly_movie.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.example.on_fly_movie.BuildConfig
import com.example.on_fly_movie.R
import com.example.on_fly_movie.models.Backdrop


class DetailsMovieImageAdapter(private val context: Context?, private val model: List<Backdrop>) : PagerAdapter() {

    private val urlToImage = BuildConfig.URL_IMAGE_W500

    override fun getCount(): Int {
        return model.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return  view == `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        // val imageView = ImageView(context)

        val layoutInflater = LayoutInflater.from(context)
        val view: View = layoutInflater.inflate(R.layout.detailt_images_list, container, false)
        val image = view.findViewById<ImageView>(R.id.gallery_img)


        //title.text = model[position].backdrop_path

        Glide.with(context!!)
            .load(urlToImage + model[position].filePath)
            .into(image)

        container.addView(view, 0)

        return  view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {

        container.removeView(`object` as View)
    }
}