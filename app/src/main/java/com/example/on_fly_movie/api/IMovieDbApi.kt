package com.example.on_fly_movie.api

import com.example.on_fly_movie.models.*
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface IMovieDbApi {

    @GET("movie/upcoming")
    fun getUpcomingMoviesAsync(
        @Query("page") page: Int?,
        @Query("perPage") perPage: Int?,
        @Query("language") language: String
    ): Deferred<Response<BaseMovieModelResponse>>


    //Trending /trending/{media_type}/{time_window}
    @GET("trending/{movie}/{week}")
    fun getTrendingMovieAsync(
        @Path("movie") movie: String,
        @Path("week") week: String
    ): Deferred<Response<BaseMovieModelResponse>>

    @GET("movie/popular")
    fun getPopularMovieAsync(
        @Query("page") page: Int?,
        @Query("perPage") perPage: Int?,
        @Query("language") language: String
    ): Deferred<Response<BaseMovieModelResponse>>


    @GET("movie/{movieType}")
    fun getPagedMoviesAsync(
        @Path(value = "movieType", encoded = true) movieType: String?,
        @Query("page") page: Int?,
        @Query("perPage") perPage: Int?,
        @Query("language") language: String
    ): Deferred<Response<BaseMovieModelResponse>>

    @GET("movie/now_playing")
    fun getNowPlayingMoviesAsync(
        @Query("page") page: Int?,
        @Query("perPage") perPage: Int?,
        @Query("language") language: String
    ): Deferred<Response<BaseMovieModelResponse>>

    @GET("movie/{movie_id}")
    fun getMovieByIdAsync(
        @Path("movie_id") movie_id: Int?,
        @Query("language") language: String
    ): Deferred<Response<DetailsMovieModel>>

    @GET("search/multi")
    fun searchMoviesAsync(
        @Query("query") query: String?,
        @Query("page") page: Int?,
        @Query("perPage") perPage: Int?,
        @Query("language") language: String

    ): Deferred<Response<BaseMovieModelResponse>>

    @GET("movie/{movie_id}/images")
    fun getDetailsMovieImagesAsync(
        @Path("movie_id") movie_id: Int?
    ): Deferred<Response<ImageDataResponse>>

    @GET("movie/{movie_id}/images")
    fun getMovieImages(
        @Path("movie_id") id: Int?
    ): Deferred<Response<ImageDataResponse>>


    @GET("movie/{movie_id}/credits")
    fun getMovieCreditsAsync(
        @Path("movie_id") id: Int?,
        @Query("language") language: String
    ): Deferred<Response<Credits>>
}