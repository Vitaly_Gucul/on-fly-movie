package com.example.on_fly_movie.handlers

import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.on_fly_movie.R
import com.example.on_fly_movie.models.BaseMovieModel
import kotlinx.android.synthetic.main.fragment_home.*

class GladeHelper  {

    fun loadImage(view: View, pathToImage: String?,
                         urlToImage: String?, intoView: ImageView?){


        Glide.with(view)
            .load(urlToImage + pathToImage)
            .into(intoView!!)

    }

    fun loadMovieImage(view: View, imagePathAndUrl: String?, intoView: ImageView?){

        Glide.with(view)
            .load(imagePathAndUrl)
            .placeholder(R.mipmap.placeholder)
            .fallback(R.mipmap.placeholder)
            .into(intoView!!)

    }
}