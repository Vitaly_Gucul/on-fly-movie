package com.example.on_fly_movie.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.on_fly_movie.BuildConfig
import com.example.on_fly_movie.R
import com.example.on_fly_movie.handlers.GladeHelper
import com.example.on_fly_movie.models.BaseMovieModel

class SearchAdapter  :  PagedListAdapter<BaseMovieModel, SearchAdapter.SearchViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SearchAdapter.SearchViewHolder {
        return SearchViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.movies_list_recycler,
                parent,
                false
            )
        )
    }


    override fun onBindViewHolder(holder: SearchAdapter.SearchViewHolder, position: Int) {
        val movie: BaseMovieModel? = getItem(position)

        holder.bind(movie)

    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<BaseMovieModel>() {
            override fun areItemsTheSame(oldItem: BaseMovieModel, newItem: BaseMovieModel): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: BaseMovieModel,
                newItem: BaseMovieModel
            ): Boolean {
                return oldItem.equals(newItem)
            }
        }
    }


    inner class SearchViewHolder(itemView: View) :  RecyclerView.ViewHolder(itemView) {
        private val glideHelper = GladeHelper()
        private val urlToImage = BuildConfig.URL_IMAGE_W500

        private var upComingPoster: ImageView? = null
        private var titleMovie: TextView? = null
        //private var r_ritle: TextView? = itemView.findViewById(R.id.r_ritle)

        init {
            upComingPoster = itemView.findViewById(R.id.movie_poster)
            titleMovie = itemView.findViewById(R.id.title_movie)

        }





        fun bind(data: BaseMovieModel?) {
            val isImageNull =  if (data!!.poster_path == null) "/test" else data.poster_path

            val movieId: Int? = data.id
            val bundle = Bundle()
            bundle.putInt("movieId", movieId!!)

            glideHelper.loadMovieImage(itemView, urlToImage + data.backdrop_path, upComingPoster)
            titleMovie?.text = data.title

            itemView.setOnClickListener{
                println("movie ID: ${data.id}")

                it.findNavController().navigate(
                    R.id.action_searchFragment_to_detailsMovieFragment,
                    bundle
                )

            }

        }

    }





}