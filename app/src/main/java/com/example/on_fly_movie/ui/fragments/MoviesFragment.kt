package com.example.on_fly_movie.ui.fragments

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.on_fly_movie.R
import com.example.on_fly_movie.adapters.MoviesAdapter
import com.example.on_fly_movie.viewmodels.MoviesViewModel
import kotlinx.android.synthetic.main.fragment_movies.*

class MoviesFragment : Fragment() {
    //private val viewModel: MoviesViewModel by viewModels()

    private val viewModel: MoviesViewModel by lazy {
        val activity = requireNotNull(this.activity) {
            "You can only access the viewModel after onActivityCreated()"
        }
        ViewModelProviders.of(this, MoviesViewModel.Factory(activity.application))
            .get(MoviesViewModel::class.java)
    }
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: MoviesAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movies, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.findViewById(R.id.rv_movies_recycler)


    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //val bundleMovieType = arguments?.let { MoviesFragmentArgs.fromBundle(it).movieType }

        val args = MoviesFragmentArgs.fromBundle(requireArguments()).movieType

        val actionBar = (activity as AppCompatActivity).supportActionBar

        println("ARGS -> ${args.name}")
        when (args.type) {
            "upcoming" -> actionBar?.title = "Upcoming"
            "popular" -> actionBar?.title = "Popular"
            "top_rated" -> actionBar?.title = "Top Rated"
            "now_playing" -> actionBar?.title = "Now Playing"
        }



        viewModel.upComingDataSource.movieTypes = args.toString() // get movies_type from Home
        initRecycle()


        viewModel.moviesBuilder.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })
    }

    private fun initRecycle() {
        adapter = MoviesAdapter()
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        //recyclerView.removeView()
        mRelativeLayout.removeAllViews()

    }
}