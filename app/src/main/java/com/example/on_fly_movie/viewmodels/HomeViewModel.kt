package com.example.on_fly_movie.viewmodels

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.on_fly_movie.factory.ApiFactory
import com.example.on_fly_movie.models.BaseMovieModel
import com.example.on_fly_movie.repository.HomeRepo
import kotlinx.coroutines.*
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

class HomeViewModel(application: Application) :
    BaseViewModel(application) {

    private var TAG = "HomeViewModel"
    private val parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Default

    private val scope = CoroutineScope(coroutineContext)

    private var repository: HomeRepo = HomeRepo(ApiFactory.tmdbApi)


    val trendingMovieImageLiveData = MutableLiveData<List<BaseMovieModel>>()
    val upComingMovieImageLiveData = MutableLiveData<List<BaseMovieModel>>()
    val populardLiveData = MutableLiveData<List<BaseMovieModel>>()
    val nowPlayingMoviesLiveData = MutableLiveData<List<BaseMovieModel>>()


    /**
     * Fetch trending movies
     */
    fun fetchTrendingMovieImage() {

        scope.launch {
            try {
                val popularMovies = repository.getTrendingMovie()
                trendingMovieImageLiveData.postValue(popularMovies)
            }catch (e: Exception){
                Log.d(TAG, "error + ${e.printStackTrace()}")
                e.printStackTrace()
            }

        }
    }


    fun fetchNowPlayingMovies(){
        try {
            scope.launch {
                val popularMovies = repository.nowPlayingMovies()
                nowPlayingMoviesLiveData.postValue(popularMovies)
            }
        }catch (e: Exception){
            Log.d(TAG, "error + ${e.printStackTrace()}")
            e.printStackTrace()
        }


    }

    fun fetchUpComingMovieImage() {
        try {
            scope.launch {
                val upComingMovie = repository.getUpComingMovie()
                upComingMovieImageLiveData.postValue(upComingMovie)
            }
        }catch (e: Exception){
            Log.d(TAG, "error + ${e.printStackTrace()}")
            e.printStackTrace()
        }

    }

    /**
     * Get request to popular api
     */
    fun fetchPopularMovieImage() {
        try {
            scope.launch {
                val popularMovie = repository.getPopularMovies()
                populardLiveData.postValue(popularMovie)
            }
        }catch (e: Exception){
            Log.d(TAG, "error + ${e.printStackTrace()}")
            e.printStackTrace()
        }

    }

    /**
     * Factory for constructing NewsViewModel with parameter
     */
    class Factory(val app: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return HomeViewModel(app) as T
            }
            throw IllegalArgumentException("Unable to construct viewmodel")
        }
    }

    override fun onCleared() {
        super.onCleared()
    }

    fun cancelAllRequests() = coroutineContext.cancel()
}