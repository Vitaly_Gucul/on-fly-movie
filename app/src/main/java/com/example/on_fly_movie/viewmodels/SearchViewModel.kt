package com.example.on_fly_movie.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.on_fly_movie.datasource.MovieDataSourceFactory
import com.example.on_fly_movie.datasource.SearchDataSourceFactory
import com.example.on_fly_movie.factory.ApiFactory
import com.example.on_fly_movie.models.BaseMovieModel
import com.example.on_fly_movie.repository.HomeRepo
import com.example.on_fly_movie.repository.UpComingMovieRepo
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class SearchViewModel(application: Application): BaseViewModel(application) {
    private val parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Default

    private val scope = CoroutineScope(coroutineContext)

    private var repository: HomeRepo = HomeRepo(ApiFactory.tmdbApi)
    val searchLiveData = MutableLiveData<List<BaseMovieModel>>()

    // FOR DATA ---
    var searchDataSource = SearchDataSourceFactory(repository = repository, scope = ioScope, qwery = ""   )

    // OBSERVABLES ---
    val searchBuilder = LivePagedListBuilder(searchDataSource, pagedListConfig()).build()


    fun searchSweets(text: String) {
        searchDataSource.queryData = text
        searchBuilder.value?.dataSource?.invalidate()
    }

    fun fetchSearchMovies(query : String){
        scope.launch {
            searchDataSource.queryData = query
           // val popularMovies = repository.searchMovieRepo(query)
            //searchLiveData.postValue(popularMovies)
            searchBuilder.value?.dataSource?.invalidate()

        }
    }

    // UTILS ---
    private fun pagedListConfig() = PagedList.Config.Builder()
        .setInitialLoadSizeHint(10)
        .setEnablePlaceholders(false)
        .setPageSize(5 * 2)
        .build()

    fun cancelAllRequests() = coroutineContext.cancel()

}