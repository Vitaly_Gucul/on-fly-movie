package com.example.on_fly_movie.viewmodels

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.on_fly_movie.datasource.MovieDataSourceFactory
import com.example.on_fly_movie.factory.ApiFactory
import com.example.on_fly_movie.models.BaseMovieModel
import com.example.on_fly_movie.models.UpComingMovieModel
import com.example.on_fly_movie.repository.UpComingMovieRepo
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

/**
 * vewmodel for MoviesFragment
 */
class MoviesViewModel(application: Application): BaseViewModel(application) {

    private val parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Default

    private val scope = CoroutineScope(coroutineContext)

    private var repository: UpComingMovieRepo = UpComingMovieRepo(ApiFactory.tmdbApi)

    // FOR DATA ---
     var upComingDataSource = MovieDataSourceFactory(repository = repository, scope = ioScope, movieType = "")


    // OBSERVABLES ---
    val moviesBuilder = LivePagedListBuilder(upComingDataSource, pagedListConfig()).build()


    // UTILS ---
    private fun pagedListConfig() = PagedList.Config.Builder()
        .setInitialLoadSizeHint(10)
        .setEnablePlaceholders(false)
        .setPageSize(5 * 2)
        .build()

    fun cancelAllRequests() = coroutineContext.cancel()

    /**
     * Factory for constructing NewsViewModel with parameter
     */
    class Factory(val app: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(MoviesViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return MoviesViewModel(app) as T
            }
            throw IllegalArgumentException("Unable to construct viewmodel")
        }
    }
}