package com.example.on_fly_movie.datasource

import android.util.Log
import androidx.paging.PageKeyedDataSource
import com.example.on_fly_movie.api.IMovieDbApi
import com.example.on_fly_movie.factory.ApiFactory
import com.example.on_fly_movie.models.BaseMovieModel
import com.example.on_fly_movie.repository.BaseRepository
import kotlinx.coroutines.*

class SearchDataSource (
    private val scope: CoroutineScope,
    private val repository: BaseRepository,
    private var PAGE: Int? = 1,
    private var PAGESIZE: Int? = 10,
    private val query: String?

) : PageKeyedDataSource<Int, BaseMovieModel>() {

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, BaseMovieModel>
    ) {
        executeQuery(PAGE, params.requestedLoadSize) {
            callback.onResult(it, null, PAGE?.plus(1))
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, BaseMovieModel>) {
        val key = if (params.key > 1) params.key - 1 else null

        executeQuery(PAGESIZE, params.requestedLoadSize) {


            callback.onResult(it, key)
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, BaseMovieModel>) {
        executeQuery(params.key , params.requestedLoadSize) {
            callback.onResult(it, params.key + 1)


        }
    }

    // FOR DATA ---
    private var supervisorJob = SupervisorJob()

    override fun invalidate() {
        super.invalidate()
        supervisorJob.cancel()   // Cancel possible running job to only keep last result searched by user
    }

    private fun executeQuery(
        page: Int?,
        perPage: Int?,
        callback: (List<BaseMovieModel>) -> Unit
    ) {

        scope.launch(getJobErrorHandler() + supervisorJob) {

            delay(100) // To handle user typing case
            val service = ApiFactory.buildService(IMovieDbApi::class.java)
            val users = repository.safeApiCall(
                call = { service.searchMoviesAsync(query,page, perPage,"ru-RU").await() },
                errorMessage = "Error Fetching SearchDataSource Movies"
            )

            callback(users!!.results)
        }


    }

    private fun getJobErrorHandler() = CoroutineExceptionHandler { _, e ->
        Log.e(SearchDataSource::class.java.simpleName, "An error happened: $e")
         //networkState.postValue(NetworkState.FAILED)
    }
}