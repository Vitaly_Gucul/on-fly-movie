package com.example.on_fly_movie.viewstates

sealed class DetailsMovieIntent :IIntent  {

    object RefreshMovie : DetailsMovieIntent()
    object FetchDetailsMovie : DetailsMovieIntent()
    object FetchImagesGallery : DetailsMovieIntent()

}
