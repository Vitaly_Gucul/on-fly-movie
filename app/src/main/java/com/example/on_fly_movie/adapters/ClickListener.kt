package com.example.on_fly_movie.adapters

import com.example.on_fly_movie.models.BaseMovieModel

interface ClickListener {
    fun onCellClickListener( data: Int)
}